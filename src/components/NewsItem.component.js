import React from 'react';
import {
  StyleSheet,
  View,
  Image,
  Text,
  Linking,
  TouchableOpacity,
} from 'react-native';
import moment from 'moment';

export default class NewsItem extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <TouchableOpacity
        key={this.props.index}
        style={styles.itemContainer}
        onPress={() => {
          Linking.openURL(this.props.url);
        }}>
        <View>
          <Image source={{uri: this.props.image}} style={styles.itemImage} />
        </View>
        <View style={styles.itemContent}>
          <View>
            <Text numberOfLines={1} style={styles.title}>
              {this.props.title}
            </Text>
          </View>
          <View>
            <Text style={styles.date}>
              By {this.props.name} on{' '}
              {moment(this.props.date).format('DD MMMM YYYY')}
            </Text>
          </View>
          <View>
            <Text numberOfLines={3} style={styles.description}>
              {this.props.description}
            </Text>
          </View>
        </View>
      </TouchableOpacity>
    );
  }
}

const styles = StyleSheet.create({
  itemContainer: {
    display: 'flex',
    flexDirection: 'row',
    backgroundColor: 'white',
    borderRadius: 15,
    padding: 10,
    margin: 10,
    elevation: 10,
    shadowColor: '#000',
    shadowOffset: {width: 0, height: 3},
    shadowOpacity: 0.5,
    shadowRadius: 5,
  },
  itemImage: {
    flex: 1,
    width: 100,
    borderRadius: 10,
  },
  itemContent: {
    flex: 3,
    height: 100,
    marginLeft: 10,
  },
  title: {
    fontFamily: 'segoe-ui',
    fontWeight: 'bold',
  },
  date: {
    marginBottom: 5,
    fontSize: 12,
    color: '#3B50EB',
  },
  description: {
    fontFamily: 'segoe-ui',
  },
});
