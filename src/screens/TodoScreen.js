import React from 'react';
import {View, Text, Button, TextInput, ScrollView} from 'react-native';
import {
  addTodoAction,
  fetchTodosAction,
  updateTodoAction,
  deleteTodoAction,
} from '../redux/action/todo';
import {connect} from 'react-redux';

class TodoScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      text: '',
    };
  }

  componentDidMount() {
    this.props.onFetchTodos();
    console.log('isi list : ', this.props.listTodos);
  }

  render() {
    return (
      <ScrollView style={{padding: 20}}>
        <View style={{marginBottom: 20}}>
          <TextInput
            placeholder="Type new task here"
            onChangeText={(text) => this.setState({text: text})}
            style={{backgroundColor: '#ffffff', padding: 10, marginBottom: 10}}
          />
          <Button
            onPress={() => {
              this.props.onAddTodo(this.state.text);
            }}
            title="ADD TO DO"
          />
        </View>

        <View style={{marginBottom: 40}}>
          {this.props.listTodos.map((item, index) => (
            <View key={index}>
              <Text>
                {item.id}. {item.text} --{' '}
                {item.completed ? 'DONE' : 'IN PROGRESS'}
              </Text>

              <Button
                onPress={() => {
                  this.props.onDeleteTodo(item);
                }}
                title="DELETE"
                color="#444"
                style={{marginBottom: 5}}
              />
              <Button
                onPress={() => {
                  this.props.onUpdateTodo(item);
                }}
                title="DONE"
                color="green"
                style={{marginTop: 5}}
              />
            </View>
          ))}
        </View>
      </ScrollView>
    );
  }
}

const mapStateToProps = (state) => ({
  listTodos: state.todos,
});

const mapDispatchToProps = (dispatch) => ({
  onFetchTodos: () => {
    console.log('call fetch');
    dispatch(fetchTodosAction('asc'));
  },
  onAddTodo: (text) => {
    console.log('add item :', text);
    dispatch(addTodoAction(text));
  },
  onUpdateTodo: (item) => {
    console.log('update item :', item);
    dispatch(updateTodoAction(item));
  },
  onDeleteTodo: (item) => {
    console.log('delete item :', item);
    dispatch(deleteTodoAction(item));
  },
});

export default connect(mapStateToProps, mapDispatchToProps)(TodoScreen);
