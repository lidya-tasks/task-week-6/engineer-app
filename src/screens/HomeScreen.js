import React from 'react';
import {ScrollView, StyleSheet, View, Text, Image} from 'react-native';
// import Ionicons from 'react-native-vector-icons/Ionicons';
import {Icon} from 'react-native-elements';

import PromotionItem from '../components/Promotion.component';
import Jokes from '../components/Jokes.component';
import Weather from '../components/Weather.component';
import {connect} from 'react-redux';

class HomeScreen extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <View>
        <View style={styles.header}>
          <View style={styles.headerImage}>
            <Image
              source={{
                uri: 'https://www.flaticon.com/aut',
              }}
              style={styles.profilImage}
            />
          </View>
          <View style={styles.headerTitle}>
            <Text style={styles.titleText}>Hi, {this.props.auth.username}</Text>
          </View>
          <View style={styles.headerIcon}>
            <Icon
              name="list-alt"
              type="font-awesome"
              color="#777"
              onPress={() => this.props.navigation.navigate('TodoScreen')}
            />
          </View>
        </View>
        <ScrollView>
          <View style={styles.body}>
            {/* Carousel */}
            <View>
              <PromotionItem />
            </View>
            {/* Jokes */}
            <View style={styles.itemJokes}>
              <Jokes />
            </View>
            {/* Weathert */}
            <View style={styles.itemJokes}>
              <Weather />
            </View>
          </View>
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  header: {
    flexDirection: 'row',
    padding: 10,
    backgroundColor: 'white',
    alignItems: 'center',
  },
  headerImage: {
    flex: 1,
    marginHorizontal: 10,
  },
  profilImage: {
    width: 30,
    height: 30,
  },
  headerTitle: {
    flex: 8,
  },
  headerIcon: {
    marginHorizontal: 10,
  },
  titleText: {
    fontFamily: 'segoe-ui',
    fontWeight: 'bold',
    color: '#333333',
    fontSize: 15,
  },
  body: {
    padding: 10,
    height: 700,
    backgroundColor: '#FFFFFF',
  },
  itemJokes: {
    marginTop: 10,
  },
});

const mapStateToProps = (state) => ({
  auth: state.auth,
});

const mapDispatchToProps = (dispatch) => ({});

export default connect(mapStateToProps, mapDispatchToProps)(HomeScreen);
