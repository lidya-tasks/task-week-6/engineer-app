import {
  GET_PROFILE,
  GET_PROFILE_SUCCESS,
  GET_PROFILE_FAILED,
} from '../action/profile_types';
import {LOGOUT} from '../action/auth_types';

// initial state = nilai awal data profile yang ada di store
const initialState = {
  isLoading: false,
  id: null,
  username: null,
  first_name: null,
  last_name: null,
  photo_url: null,
};

const profile = (state = initialState, action) => {
  switch (action.type) {
    case GET_PROFILE: {
      return {
        ...state,
        isLoading: true,
      };
    }
    case GET_PROFILE_SUCCESS: {
      return {
        ...state,
        ...action.payload,
        isLoading: false,
      };
    }
    case GET_PROFILE_FAILED: {
      return {
        ...state,
        isLoading: false,
      };
    }
    case LOGOUT: {
      return {
        isLoading: false,
        id: null,
        username: null,
        first_name: null,
        last_name: null,
        photo_url: null,
      };
    }
    default:
      return state;
  }
};

export default profile;
