import {GET_CONTENT} from './content_types';

export const getContent = (interest) => {
  return {
    type: GET_CONTENT,
    interest,
  };
};
