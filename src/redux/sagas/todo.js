import {
  FETCH_TODOS,
  FETCH_SUCCEEDED,
  FETCH_FAILED,
  ADD_TODO,
  UPDATE_TODO,
  DELETE_TODO,
} from '../action/todo';

import {put, takeLatest} from 'redux-saga/effects';

const urlGetTodos = 'https://backend-glints-app.herokuapp.com/todos';
const urlPostTodo = 'https://backend-glints-app.herokuapp.com/todos';
const urlUpdateTodo = 'https://backend-glints-app.herokuapp.com/todos';
const urlDeleteTodo = 'https://backend-glints-app.herokuapp.com/todos';

// Index
function* fetchTodos() {
  try {
    const receivedTodos = yield fetch(urlGetTodos).then((res) => res.json());
    console.log('receivedTodos: ', receivedTodos);
    yield put({type: FETCH_SUCCEEDED, receivedTodos: receivedTodos});
  } catch (error) {
    yield put({type: FETCH_FAILED, error});
  }
}

export function* watchFetchTodos() {
  console.log('watchFetchTodos()');
  yield takeLatest(FETCH_TODOS, fetchTodos);
}

// Create
function* addTodo(action) {
  try {
    const response = yield fetch(urlPostTodo, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        text: action.newTodo.text,
        completed: action.newTodo.completed,
      }),
    });

    console.log('response: ', response);
    yield put({type: FETCH_TODOS, sort: 'asc'});
  } catch (error) {
    yield put({type: FETCH_FAILED, error});
  }
}

export function* watchAddTodo() {
  console.log('watchAddTodo()');
  yield takeLatest(ADD_TODO, addTodo);
}

// Update
function* updateTodo(action) {
  try {
    const response = yield fetch(
      urlUpdateTodo + '/' + action.todo.id.toString(),
      {
        method: 'PUT',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          text: action.todo.text,
          completed: true,
        }),
      },
    );

    console.log('response: ', response);
    yield put({type: FETCH_TODOS, sort: 'asc'});
  } catch (error) {
    yield put({type: FETCH_FAILED, error});
  }
}

export function* watchUpdateTodo() {
  console.log('watchUpdateTodo()');
  yield takeLatest(UPDATE_TODO, updateTodo);
}

// Delete
function* deleteTodo(action) {
  try {
    const response = yield fetch(
      urlDeleteTodo + '/' + action.todo.id.toString(),
      {
        method: 'DELETE',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
        },
        body: {},
      },
    );

    console.log('response: ', response);
    yield put({type: FETCH_TODOS, sort: 'asc'});
  } catch (error) {
    yield put({type: FETCH_FAILED, error});
  }
}

export function* watchDeleteTodo() {
  console.log('watchDeleteTodo()');
  yield takeLatest(DELETE_TODO, deleteTodo);
}
