// import {all, fork} from 'redux-saga/effects';

// inefficient way
// import {
//   watchFetchTodos,
//   watchAddTodo,
//   watchUpdateTodo,
//   watchDeleteTodo,
// } from './todo';

// export default function* index() {
//   console.log('rootSaga');
//   yield all([
//     fork(watchFetchTodos),
//     fork(watchAddTodo),
//     fork(watchUpdateTodo),
//     fork(watchDeleteTodo),
//   ]);
// }

// import * as todoSagas from './todo';
// export default function* index() {
//   console.log('rootSaga');
//   yield all([...Object.values(todoSagas)].map(fork));
// }

import {all} from 'redux-saga/effects';
import authSaga from './auth';
import profileSaga from './profile';
import interestSaga from './interest';

export default function* rootSaga() {
  yield all([authSaga(), profileSaga(), interestSaga()]);
}
